#!/usr/bin/python3

import sys

from subprocess import Popen, PIPE

caller_id = sys.argv[1]
recording_filename = sys.argv[2]

ff_proc = Popen(["ffmpeg", "-i", recording_filename, "-f", "ogg", "-"], stdout=PIPE, shell=False)
recording_ogg, _ = ff_proc.communicate(timeout=None)

with open(f"/usr/share/asterisk/moh/custom/{caller_id}.ogg", "wb") as f:
    f.write(recording_ogg)

# template = """
# Звонили с номера {caller_id}:
#
# — Привет! Я не Олег. Запишите сообщение после сигнала.
# — {message_transcribed}
# """
