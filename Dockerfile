FROM bitnami/minideb:bullseye

# This installs Asterisk 16, which is supported until 2023... good enough for now
# https://wiki.asterisk.org/wiki/display/AST/Asterisk+Versions
RUN install_packages asterisk asterisk-voicemail ffmpeg python3
