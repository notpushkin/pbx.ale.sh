Everything except `moh/` and `sounds/` is licensed under the **ISC License**:

    Copyright 2022 Alexander Pushkov <alexander@notpushk.in>

    Permission to use, copy, modify, and/or distribute this software for any
    purpose with or without fee is hereby granted, provided that the above
    copyright notice and this permission notice appear in all copies.

    THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
    WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
    MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
    SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
    WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
    ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
    IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.


`moh/` is left blank right now. You'll want to put your own hold music there,
check licensing on that first. If I ever add example music I'll update this
with its licensing information.

`sounds/` are the prompts with super proprietary punchlines that I use for my
own phone, please replace them with something else. (I'll probably add some
generic ones instead, but I'm not sure about how licensing works if I generate
these with some TTS.)
