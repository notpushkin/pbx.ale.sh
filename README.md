# pbx.ale.sh

A tiny one-user PBX. Work in progress.

# Usage

```
docker-compose up
```

Then connect using any SIP softphone:

- **Username**: `6001` or `6002`
- **Password**: `unsecurepassword`
- **Domain**: `localhost` (or your IP)
- **Proxy**: leave blank or same as Domain

You may have to set the SIP port on the softphone to `5061` or something else,
since Asterisk will already be using the standard SIP port `5060` for its own
SIP connections.

To reload configuration (perhaps use with `inotifywatch`?):

```
docker-compose exec asterisk asterisk -x reload
```

To bring up the Asterisk CLI:

```
docker-compose exec asterisk rasterisk
```

### Music on hold

```
ffmpeg -i [path/to/file.flac] -ac 1 -ab 128k -ar 8000 -acodec pcm_s16le moh/[artist_name-track_name].wav
```

Probably should also add G722 (16k) version, need to research:

```
ffmpeg -i [path/to/file.flac] -ar 16000 -acodec g722 [artist_name-track_name].g722
```

The same commands can be used to convert sounds.


## Deployment

TODO
